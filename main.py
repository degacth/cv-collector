import mechanicalsoup
import click


@click.command()
@click.option('--username', help='Username', prompt='Input username')
@click.option('--password', help='Password', prompt='Input user password')
def main(username, password):
    browser = mechanicalsoup.StatefulBrowser(soup_config={'features': 'lxml'})

    browser.session.headers.update({
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) '
                      'AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/61.0.2228.0 Safari/537.36'
    })
    browser.open('https://hh.ru/account/login')

    browser.select_form('.light-page-content form')
    browser['username'] = username
    browser['password'] = password
    browser.submit_selected()

    browser.select_form('form[action="/search/resume"]')
    browser['text'] = 'Программист Java'
    browser.submit_selected()

    for cv_link in browser.get_current_page().select('.resume-search-item__header a'):
        print(cv_link.text, cv_link.attrs['href'])

    # browser.launch_browser()


if __name__ == '__main__':
    main()
